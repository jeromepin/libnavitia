Libnavitia
===========

Libnavitia is a small library to interact with [Navitia.io](https://www.navitia.io/) API.


## Installation

```bash
npm install https://gitlab.com/jeromepin/libnavitia.git
```

## Usage

```js
var navitia = require('libnavitia');

navitia.configure({
    token: "YOUR_TOKEN",
});

navitia.query()
        .coverage()
        .region('fr-idf')
        .journey(new navitia.GeoPoint(48.8467927, 2.3749036), new navitia.GeoPoint(48.8583736, 2.2922926))
        .then(function(response) {}).catch(function(err) {});


navitia.query()
        .journey(new navitia.GeoPoint(48.8467927, 2.3749036), new navitia.GeoPoint(48.8583736, 2.2922926))
        .then(function(response) {}).catch(function(err) {});
```


## Tests

```bash
npm test
```
