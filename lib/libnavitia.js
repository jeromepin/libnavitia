var axios = require('axios');

'use strict';

/**
 * Describe a Geographic Point
 * @param {Float} latitude
 * @param {Float} longitude
 */
var GeoPoint = function (latitude, longitude) {
    this.latitude(latitude);
    this.longitude(longitude);

    return this;
};

/**
 * Get/Set object's latitude and check for value's validity
 * @param  {Float} lat [description]
 * @return {Float}     [description]
 */
GeoPoint.prototype.latitude = function (lat) {
    if (lat)
        if (lat < 0 || lat > 90)
            throw 'Latitude must be between 0 and 90.';

        this._latitude = lat;

    return this._latitude;
};

/**
 * Get/Set object's longitude and check for value's validity
 * @param  {Float} lng [description]
 * @return {Float}     [description]
 */
GeoPoint.prototype.longitude = function (lng) {
    if (lng)
        if (lng < -180 || lng > 180)
            throw 'Longitude must be between -180 and 180.';

        this._longitude = lng;

    return this._longitude;
};

GeoPoint.prototype.toNavitiaCompliantString = function () {
    return encodeURIComponent(this._longitude.toString() + ';' + this._latitude.toString());
};




/**
 * Initialize an object to query the API
 * @param {String} url     [description]
 * @param {Object} options [description]
 */
function Query(url, options) {
    this._url       = url;
    this._options   = options;
}

Query.prototype.coverage = function () {
    this._url += '/coverage';
    return this;
};

Query.prototype.region = function (region) {
    this._region = region;
    this._url   += '/' + this._region;
    return this;
};

/**
 * [journey description]
 * @param  {navitia.GeoPoint} from     [description]
 * @param  {navitia.GeoPoint} to       [description]
 * @param  {[type]} datetime [description]
 * @return {[type]}          [description]
 *
 * http://doc.navitia.io/#journeys
 * datetime=      (format: YYYYMMDDThhmmss)
 * datetime_represents=datetime
 */
Query.prototype.journey = function (from, to, datetime) {
    this._from  = from;
    this._to    = to;
    this._url += '/journeys?from=' + this._from.toNavitiaCompliantString() + '&to=' + this._to.toNavitiaCompliantString();

    return this.doRequest();
};

/**
 * Execute the real request
 * @return {Promise} [description]
 */
Query.prototype.doRequest = function () {
    return axios.get(this._url, {
        headers: {
            'Authorization': this._options.token
        }
    });
};



var navitia = {
    _protocol  : 'https',
    _endpoint  : 'api.navitia.io',
    _version   : 'v1',
    _token     : null,
    _options   : null,

    configure: function (options) {
        this._options   = options;
    },

    query: function () {
        if (this._options.token == null || this._options.token === undefined)
            throw 'Token should be defined';

        var url =  this._protocol + '://' + this._endpoint + '/' + this._version;
        return (new Query(url, this._options));
    },

    GeoPoint: GeoPoint,
};


module.exports = navitia;
